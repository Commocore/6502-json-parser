﻿
JsonParser .block

; @access public
; @return void
setAsciiMode
	lda #0
	sta storeJsonValue.asciiToPetsciiMode + 1
rts


; @access public
; @return void
setPetsciiMode
	lda #1
	sta storeJsonValue.asciiToPetsciiMode + 1
rts


; @access public
; @param X - lo-address byte location of key name
; @param Y - hi-address byte location of key name
; @param A - key name length
; @return void
setExpectededJsonKey
	stx doesExpectedKeyMatch.expectedKey + 1
	sty doesExpectedKeyMatch.expectedKey + 2
	cmp #JSON_KEY_MAX_LENGTH
	bcc +
		lda #JSON_KEY_MAX_LENGTH
+
	sta doesExpectedKeyMatch.keyLength + 1
rts


; Used for setting JSON object for static parsing
; 
; @access public
; @param X
; @param Y
; @noreturn passes to prepareParser
setJsonObject
	stx jsonObjectMemoryLocation + 0
	sty jsonObjectMemoryLocation + 1

	; Decrement pointers to static JSON object by one as walkForward will increment it at the start anyways
	; This way, static parsing behaves the same as streamed parsing and that helps with the development of this parser
	lda jsonObjectMemoryLocation + 0
	sec
	sbc #1
	sta jsonObjectMemoryLocation + 0
	lda jsonObjectMemoryLocation + 1
	sbc #0
	sta jsonObjectMemoryLocation + 1
	
	; Continue intentionally


; @access public
; @return void
prepareParser
	lda #0
	sta currentNestingLevel
	sta storeJsonValue.asciiToPetsciiMode + 1
	sta jsonKeyLoaded
	sta jsonKeyBufferLength
	sta byteLoaded
	sta keyFoundInCurrentJsonObjectIterator
	sta repeatKeySearchingInCurrentJsonObjectIterator
rts


; @access public
; @param X - lo-address byte of pointer location
; @param Y - hi-address byte of pointer location
; @return void
setJsonOutputMemoryLocationPointer
	stx setJsonOutputMemoryLocation.pointerLo + 1
	sty setJsonOutputMemoryLocation.pointerLo + 2
	stx setJsonOutputMemoryLocation.pointerHi + 1
	sty setJsonOutputMemoryLocation.pointerHi + 2
rts


; @access public
; @param X - lo-address byte of output location
; @param Y - hi-address byte of output location
; @return void
setJsonOutputMemoryLocation .block

pointerLo
	stx $1234
	
	tya
	ldx #1
pointerHi
	sta $1234,x
rts


.bend


; @access public
; @param X - lo-address byte location of method iterator
; @param Y - hi-address byte location of method iterator
; @return void
setMethodIterator
	stx advanceToMethodIterator.iteratorPointers + 1
	sty advanceToMethodIterator.iteratorPointers + 2
rts


; Adjust pointers for currently used table for the next key
; 
; @access public
; @param X - <size
; @param Y - >size
; @uses storeJsonKeyFromBuffer.pointer
; @return void
moveStoreJsonKeyPointerLocation .block
	stx sizeLo + 1
	sty sizeHi + 1

	lda storeJsonKeyFromBuffer.pointer + 1
	sta pointerInLo + 1
	sta pointerOutLo + 1
	sta pointerInHi + 1
	sta pointerOutHi + 1

	lda storeJsonKeyFromBuffer.pointer + 2
	sta pointerInLo + 2
	sta pointerOutLo + 2
	sta pointerInHi + 2
	sta pointerOutHi + 2
	
	clc
	ldy #0
	
pointerInLo
	lda $1234,y
sizeLo
	adc #0
pointerOutLo
	sta $1234,y
	
	iny
pointerInHi
	lda $1234,y
sizeHi
	adc #0
pointerOutHi
	sta $1234,y

rts

.bend


; Adjust pointers for currently used length table for the next key
; 
; @access public
; @param X - <size
; @param Y - >size
; @uses storeJsonKeyLength.pointer
; @return void
moveStoreJsonKeyLengthPointerLocation .block
	stx sizeLo + 1
	sty sizeHi + 1

	lda storeJsonKeyLength.pointer + 1
	sta pointerInLo + 1
	sta pointerOutLo + 1
	sta pointerInHi + 1
	sta pointerOutHi + 1

	lda storeJsonKeyLength.pointer + 2
	sta pointerInLo + 2
	sta pointerOutLo + 2
	sta pointerInHi + 2
	sta pointerOutHi + 2
	
	clc
	ldy #0
	
pointerInLo
	lda $1234,y
sizeLo
	adc #0
pointerOutLo
	sta $1234,y
	
	iny
pointerInHi
	lda $1234,y
sizeHi
	adc #0
pointerOutHi
	sta $1234,y

rts

.bend


; Adjust pointers for currently used table for the next value
; 
; @access public
; @param X - <size
; @param Y - >size
; @uses storeJsonValue.pointer
; @return void
moveStoreJsonValuePointerLocation .block
	stx sizeLo + 1
	sty sizeHi + 1

	lda storeJsonValue.pointer + 1
	sta pointerInLo + 1
	sta pointerOutLo + 1
	sta pointerInHi + 1
	sta pointerOutHi + 1

	lda storeJsonValue.pointer + 2
	sta pointerInLo + 2
	sta pointerOutLo + 2
	sta pointerInHi + 2
	sta pointerOutHi + 2
	
	clc
	ldy #0
	
pointerInLo
	lda $1234,y
sizeLo
	adc #0
pointerOutLo
	sta $1234,y
	
	iny
pointerInHi
	lda $1234,y
sizeHi
	adc #0
pointerOutHi
	sta $1234,y

rts

.bend


; Adjust pointers for currently used length table for the next value
; 
; @access public
; @param X - <size
; @param Y - >size
; @uses storeJsonValueLength.pointer
; @return void
moveStoreJsonValueLengthPointerLocation .block
	stx sizeLo + 1
	sty sizeHi + 1

	lda storeJsonValueLength.pointer + 1
	sta pointerInLo + 1
	sta pointerOutLo + 1
	sta pointerInHi + 1
	sta pointerOutHi + 1

	lda storeJsonValueLength.pointer + 2
	sta pointerInLo + 2
	sta pointerOutLo + 2
	sta pointerInHi + 2
	sta pointerOutHi + 2
	
	clc
	ldy #0
	
pointerInLo
	lda $1234,y
sizeLo
	adc #0
pointerOutLo
	sta $1234,y
	
	iny
pointerInHi
	lda $1234,y
sizeHi
	adc #0
pointerOutHi
	sta $1234,y

rts

.bend


; @access private
; @param A - character
; @return X - zero flag status (use bne for true)
isEndOfValue
	ldx stringValue
	beq isEndOfIntegerValue
	
isEndOfStringValue
	cmp #QUOTE_CHAR
	beq +
		ldx #FALSE
		rts
+
	ldx #TRUE
	rts

	
isEndOfIntegerValue
	cmp #RIGHT_CURLY_BRACKET_CHAR
	beq +
		cmp #RIGHT_SQUARE_BRACKET_CHAR
		beq +
			cmp #COMMA_CHAR
			beq +
				ldx #FALSE
				rts
+
	ldx #TRUE
rts


; @access private
; @return X - zero flag status (use bne for true, occurs for square bracket or left curly bracket character)
isJsonArrayOrObject
	cmp #LEFT_SQUARE_BRACKET_CHAR
	bne +
		ldx #TRUE
		rts
+
	cmp #LEFT_CURLY_BRACKET_CHAR
	bne +
		ldx #TRUE
		rts
+
	ldx #FALSE
rts


; @access private
; @return void
fastForwardSpaceCharacters
-
	jsr getByteProcedure
	cmp #SPACE_CHAR
	bne +
		jsr walkForwardProcedure
		jmp -
+
rts


.include "includes/constants.asm"
.include "includes/variables.asm"
.include "includes/tables.asm"
.include "includes/parse-static-object.asm"
.include "includes/walk-forward-procedure.asm"
.include "includes/get-byte-procedure.asm"
.include "includes/expect-any-json-key.asm"
.include "includes/expect-json-key.asm"
.include "includes/look-for-buffer-key.asm"
.include "includes/does-expected-key-match.asm"
.include "includes/store-json-value.asm"
.include "includes/store-json-value-length.asm"
.include "includes/store-json-key-length.asm"
.include "includes/advance-to-method-iterator.asm"
.include "includes/walk-forward-to-next-json-element.asm"
.include "includes/is-json-array-completed.asm"
.include "includes/is-json-object-completed.asm"
.include "includes/exit-from-json-array-or-object-loop.asm"
.include "includes/convert-ascii-to-petscii.asm"


.if DEBUG
	.include "includes/debug-static-parsing.asm"
.fi

.bend


.include "includes/macros.asm"
