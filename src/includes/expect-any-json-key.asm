﻿
; @access public
; @needs advanceToMethodIterator.iteratorPointers to be set before
; @return void
expectAnyJsonKey
	jsr getByteProcedure
	cmp #RIGHT_CURLY_BRACKET_CHAR
	bne +
		; End of the JSON object has been reached, so let's ignore all expected JSON keys until isJsonObjectCompleted will be called
		rts
+

	lda #0
	sta jsonKeyBufferLength
	sta storeJsonKeyLength.length + 1
	jsr lookForBufferKey
	jsr advanceToMethodIterator
rts
