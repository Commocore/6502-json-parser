﻿
.weak

; If you want to use zero-page addressing for variables instead, just overwrite the labels below

jsonKeyLoaded
	.byte 0
	

jsonKeyBufferLength
	.byte 0


bufferingMode
	.byte 0

	
currentNestingLevel
	.byte 0


stringValue
	.byte 0
	

byteLoaded
	.byte 0
	
	
keyFoundInCurrentJsonObjectIterator
	.byte 0
	
	
repeatKeySearchingInCurrentJsonObjectIterator
	.byte 0


.if DEBUG

debugLastReadByteLocation = $fb ; word
debugBufferCursorPosition = $fd ; word
debugCurrentNestingLevelLocation = $ff

.fi


.endweak
