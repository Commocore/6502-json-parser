﻿
; @access public
; @param X - <outputMemoryPointer
; @param Y - >outputMemoryPointer
; @return void
storeJsonValue .block
	stx pointer + 1
	sty pointer + 2
	
	ldy #1
pointer
	lda $1234,y
	sta output + 1,y
	dey
	bpl pointer
	
	lda output + 1
	sta storeJsonValueLength.beginLo + 1
	lda output + 2
	sta storeJsonValueLength.beginHi + 1
	
	ldy #0
	sty stringValue
	
	jsr walkForwardToTheBeginningOfJsonElement
	
	jsr isJsonArrayOrObject
	beq ++
		; Yes, this is JSON array or object
		jsr walkForwardToTheBeginningOfJsonElement
		
		; Is it an empty array?
		cmp #RIGHT_SQUARE_BRACKET_CHAR
		bne +
			jsr walkForwardProcedure
			rts

+
		; Is it an empty object?
		cmp #RIGHT_CURLY_BRACKET_CHAR
		bne +
			jsr walkForwardProcedure
			rts

+

	; Is it a string value?
	cmp #QUOTE_CHAR
	bne ++
		ldx #1
		stx stringValue
		jsr walkForwardProcedure
	
-
	jsr getByteProcedure
	jsr isEndOfValue
	beq ++
		; It's the end of a value
		ldx stringValue
		beq +
			jsr walkForwardProcedure ; walk forward if it was a string value to skip the quote character
+
		
		jsr fastForwardSpaceCharacters
		
		; Exiting from the procedure of storing the value
		rts
+
	; Escaped character?
	cmp #BACKSLASH_CHAR
	bne +
		jsr walkForwardProcedure
		jsr getByteProcedure
+

asciiToPetsciiMode
	ldx #0 ; value modified on the fly
	beq +
		jsr convertAsciiToPetscii
+
output
	sta $1234
	
nxt
	inc output + 1
	bne +
		inc output + 2
+
	
	jsr walkForwardProcedure
	
	jmp -


.bend


; @access private
; @return A - character value
walkForwardToTheBeginningOfJsonElement
-
	jsr walkForwardProcedure
	jsr getByteProcedure
	cmp #QUOTE_CHAR
	bne +
		rts
+
	cmp #LEFT_SQUARE_BRACKET_CHAR
	bne +
		rts
+
	cmp #LEFT_CURLY_BRACKET_CHAR
	bne +
		rts
+
	; Might be an empty array or the end of an array
	cmp #RIGHT_SQUARE_BRACKET_CHAR
	bne +
		rts
+
	; Might be an empty object or the end of an object
	cmp #RIGHT_CURLY_BRACKET_CHAR
	bne +
		rts
+
	; Is it a digit value?
	cmp #DIGITS_FIRST_CHAR
	bcc +
		cmp #DIGITS_LAST_CHAR + 1
		bcs +
			rts
+

	; Any of allowed characters, move forward...
	
	jmp -
