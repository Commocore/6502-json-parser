﻿
.weak

; Feel free to write your own implementation for static parsing
; For streamed parsing you have to write your own implementation that:
; - on walkForward: fetches byte from stream and fills into a custom buffer
; - on getByte: reads the last byte from a custom buffer
; 
; Note: If Y register is used in your implementation, you have to save it, as JSON Parser uses it as well
; 
; @access private
; @return A - value
getByte
	lda $1234 ; modified on fly
rts


; @access private
; @modifies getByte + 1
; @modifies getByte + 2
; @return void
walkForward
	inc getByte + 1
	bne +
		inc getByte + 2
+

	.if DEBUG
		jsr trackBufferCursorPosition
		jsr trackLastReadByte
	.fi
rts


.endweak
