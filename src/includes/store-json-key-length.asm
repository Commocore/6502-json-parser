﻿
; @access public
; @param X - <outputMemoryPointer
; @param Y - >outputMemoryPointer
; @return void
storeJsonKeyLength .block
	stx pointer + 1
	sty pointer + 2
	
	ldy #1
pointer
	lda $1234,y
	sta output + 1,y
	dey
	bpl pointer

length
	lda #0
	
output
	sta $1234
rts

.bend
