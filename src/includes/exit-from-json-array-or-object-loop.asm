﻿
; @access private
; @modifies jsonKeyLoaded
; @modifies jsonKeyBufferLength
; @return A - zero flag status (use bne for true)
exitFromJsonArrayOrObjectLoop
	lda #0
	sta jsonKeyLoaded
	sta jsonKeyBufferLength
	
	lda #EXIT_FROM_JSON_ARRAY_OR_OBJECT_LOOP
rts
