﻿
; @access private
; @modified byteLoaded
; @return void
walkForwardProcedure
	jsr walkForward

	lda #1
	sta byteLoaded

	jsr getByteProcedure
	cmp #LEFT_CURLY_BRACKET_CHAR
	bne +
		inc currentNestingLevel
		rts
+
	cmp #RIGHT_CURLY_BRACKET_CHAR
	bne +
		dec currentNestingLevel
		rts
+
	cmp #LEFT_SQUARE_BRACKET_CHAR
	bne +
		inc currentNestingLevel
		rts
+
	cmp #RIGHT_SQUARE_BRACKET_CHAR
	bne +
		dec currentNestingLevel
+
rts
