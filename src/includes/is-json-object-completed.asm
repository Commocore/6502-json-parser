﻿
; @access public
; @return A - zero flag status (use beq for true)
isJsonObjectCompleted

	jsr fastForwardSpaceCharacters

	jsr getByteProcedure
	cmp #RIGHT_CURLY_BRACKET_CHAR
	bne ++
		; Reset key found in current iterator
		lda #0
		sta keyFoundInCurrentJsonObjectIterator
		
		lda currentNestingLevel
		beq +
			jsr walkForwardProcedure
+
		jmp exitFromJsonArrayOrObjectLoop
+

	lda keyFoundInCurrentJsonObjectIterator
	bne ++
		lda repeatKeySearchingInCurrentJsonObjectIterator
		bne +
			; Key found in the JSON object was not found but might be expected at the top of the iterator, so let's repeat it for once
			inc repeatKeySearchingInCurrentJsonObjectIterator

			lda #DO_NOT_EXIT_FROM_JSON_ARRAY_OR_OBJECT_LOOP
			rts
+
		; Key found in the JSON object was not found in the iterator, let's walk forward
		lda #0
		sta repeatKeySearchingInCurrentJsonObjectIterator
		
		jsr walkForwardToNextJsonElement
		
		jsr getByteProcedure
		cmp #RIGHT_CURLY_BRACKET_CHAR
		bne +
			jsr walkForwardProcedure
			
			jmp exitFromJsonArrayOrObjectLoop
+
	
	lda #DO_NOT_EXIT_FROM_JSON_ARRAY_OR_OBJECT_LOOP
rts
