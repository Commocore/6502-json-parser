﻿
; @access private
; @return A - byte
getByteProcedure
	lda byteLoaded
	bne +
		; If it's the first run ever, byte won't be loaded into memory
		jsr walkForwardProcedure
+
	jsr getByte
rts
