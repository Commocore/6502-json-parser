﻿
; @access public
; @param X - <outputMemoryPointer
; @param Y - >outputMemoryPointer
; @return void
storeJsonValueLength .block
	stx pointer + 1
	sty pointer + 2
	
	ldy #1
pointer
	lda $1234,y
	sta output1 + 1,y
	sta output2 + 1,y
	dey
	bpl pointer

	sec
	ldy #0
	lda storeJsonValue.output + 1
beginLo
	sbc #0
output1
	sta $1234,y
	
	iny
	lda storeJsonValue.output + 2
beginHi
	sbc #0
output2
	sta $1234,y
rts

.bend
