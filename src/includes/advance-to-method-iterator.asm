﻿
; @access private
; @return void
advanceToMethodIterator .block
	
	; Reset the key loaded so another key expectation can be performed
	ldy #0
	sty jsonKeyLoaded
	
	lda #1
	sta keyFoundInCurrentJsonObjectIterator
	
iteratorPointers
	jsr $1234
+
rts

.bend
