﻿
; @access private
; @return void
lookForBufferKey
	ldy #0
	sty bufferingMode
	
-
	jsr walkForwardProcedure
	jsr getByteProcedure
	cmp #LEFT_SQUARE_BRACKET_CHAR
	bne +
		jmp continue
+
	cmp #LEFT_CURLY_BRACKET_CHAR
	bne +
		jmp continue
+
	cmp #RIGHT_SQUARE_BRACKET_CHAR
	bne +
		jmp continue
+
	cmp #RIGHT_CURLY_BRACKET_CHAR
	bne +
		jmp continue
+

	; Escaped character? If yes then treat as regular character, not function one
	cmp #BACKSLASH_CHAR
	bne +
		jsr walkForwardProcedure
		jsr getByteProcedure
		jmp ++
+
	cmp #QUOTE_CHAR
	bne +
		inc bufferingMode
		lda bufferingMode
		cmp #2
		bne -
		
		; JSON key loaded into the buffer
		jmp bufferKeyLoaded ; no need to return here
+

	
continue
	ldx bufferingMode
	beq - ; only continue if equal to 1
	
	; Getting bytes of the JSON key name between the quotes
	
	sta jsonKeyBuffer,y
	iny
	cpy #JSON_KEY_MAX_LENGTH
	bne -
	
	; Allowed size of the JSON key exceeded, move forward to the end of the JSON key name but ignore any new bytes
	jsr walkForwardToTheEndOfKey
	
	; Continue intentionally
	

; @access private
; @param Y - key buffer length
; @sets jsonKeyLoaded
; @return void
bufferKeyLoaded
	sty jsonKeyBufferLength
	sty storeJsonKeyLength.length + 1
	
	ldy #1
	sty jsonKeyLoaded
	
	jsr walkForwardProcedure ; walk forward to the colon character, it's time to read the value if the JSON key matches with the expected one
rts


; @access public
; @param X - <outputMemoryPointer
; @param Y - >outputMemoryPointer
; @return void
storeJsonKeyFromBuffer .block
	stx pointer + 1
	sty pointer + 2
	
	ldy #1
pointer
	lda $1234,y
	sta output + 1,y
	dey
	bpl pointer

	ldy #0
-
	lda jsonKeyBuffer,y
output
	sta $1234,y
	iny
	cpy jsonKeyBufferLength
	bne -
	
	lda #0
	sta jsonKeyBufferLength
rts

.bend


; @access private
; @return void
walkForwardToTheEndOfKey
-
	jsr walkForwardProcedure
	jsr getByteProcedure
	cmp #QUOTE_CHAR
	bne +
		rts
+
	jmp -
rts
