﻿
; Ignoring JSON element with all potential objects nested inside in case the JSON key was not expected
; 
; @access private
; @return void
walkForwardToNextJsonElement .block
	lda #0
	sta stringValue
	sta arrayNesting
	sta objectNesting

-
	jsr walkForwardProcedure
	jsr getByteProcedure
	
	ldx stringValue
	beq +
		; String mode
		cmp #QUOTE_CHAR
		bne -
			ldx #0
			stx stringValue
			jmp -
+
	; Non-string mode
	cmp #QUOTE_CHAR
	bne +
		ldx #1
		stx stringValue
		jmp -
+
	cmp #LEFT_SQUARE_BRACKET_CHAR
	bne +
		inc arrayNesting
		jmp -
+
	cmp #RIGHT_SQUARE_BRACKET_CHAR
	bne +
		lda arrayNesting
		beq exit
			dec arrayNesting
			jsr isCurrentJsonObjectClosing
			bne exit
				jmp -
+
	cmp #LEFT_CURLY_BRACKET_CHAR
	bne +
		inc objectNesting
		jmp -
+
	cmp #RIGHT_CURLY_BRACKET_CHAR
	bne +
		lda objectNesting
		beq exit
			dec objectNesting
			jsr isCurrentJsonObjectClosing
			bne exit
				jmp -
+
	cmp #COMMA_CHAR
	bne -
		jsr isCurrentJsonObjectClosing
		bne exit
		jmp -
		
	jmp exit
	

exit
	lda #0
	sta jsonKeyLoaded
	sta jsonKeyBufferLength
	
	rts
	

; The number of nested arrays and objects should be equal to zero
; If this is not the case, then parser is still going through an ignored JSON object
; 
; @access private
; @return A - zero flag status (use bne for true)
isCurrentJsonObjectClosing
	lda objectNesting
	ora arrayNesting
	bne +
		lda #TRUE
		rts
+
	lda #FALSE
rts


arrayNesting
	.byte 0


objectNesting
	.byte 0

	
.bend
