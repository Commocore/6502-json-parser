﻿
; @access private
; @return A - true if key from the buffer matches with expected key (use bne for true)
doesExpectedKeyMatch .block
	lda keyLength + 1
	cmp jsonKeyBufferLength
	beq +
		lda #0
		sta keyFoundInCurrentJsonObjectIterator
		
		; Expected JSON key in the buffer and the currently parsed key are definitely different as the length doesn't match
		; Returning to user-defined iterator to expect the next object or the end of objects
		lda #FALSE
		rts
+

	ldy #0
-

expectedKey
	lda $1234,y
	cmp jsonKeyBuffer,y
	beq +
		lda #0
		sta keyFoundInCurrentJsonObjectIterator
		
		; Expected JSON key in the buffer doesn't match currently parsed key
		; Returning to user-defined iterator as above
		lda #FALSE
		rts
+

	iny
keyLength
	cpy #0
	bne -
	
	lda #TRUE
rts

.bend
