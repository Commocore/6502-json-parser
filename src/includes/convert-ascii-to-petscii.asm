﻿
; @access private
; @input A - input ASCII character
; @return A - output PETSCII character
convertAsciiToPetscii
	cmp #$61 ; convert to lowercase
	bcc +
		sec
		sbc #$60
		rts
+
	cmp #AT_CHAR
	bne +
		lda #AT_PETSCII_CHAR
		rts
+
	cmp #CARET_CHAR
	bne +
		lda #UP_ARROW_PETSCII_CHAR
		rts
+
	cmp #LEFT_SQUARE_BRACKET_CHAR
	bne +
		lda #LEFT_SQUARE_BRACKET_PETSCII_CHAR
		rts
+
	cmp #RIGHT_SQUARE_BRACKET_CHAR
	bne +
		lda #RIGHT_SQUARE_BRACKET_PETSCII_CHAR
		rts
+
	cmp #UNDERSCORE_CHAR
	bne +
		lda #UNDERSCORE_PETSCII_CHAR
		rts
+
rts
