﻿
.weak

.cerror !STATIC_PARSING, "Tracking last byte read cannot be used for streamed parsing as implementation vary"


; Extra debugging information for static parsing only about the current position in JSON object and current nesting level
; For streamed parsing, follow your own implementation of walkForward buffering as you have to track the last loaded byte from the stream
; 
; @access public
; @return void
trackLastReadByte
	pha
	
	lda JsonParser.getByte + 1
	sta debugLastReadByteLocation + 0
	
	lda JsonParser.getByte + 2
	sta debugLastReadByteLocation + 1
	
	lda JsonParser.currentNestingLevel
	sta debugCurrentNestingLevelLocation
	
	pla
rts


; @access public
; @return void
trackBufferCursorPosition
	pha
	
	lda JsonParser.getByte + 1
	sta debugBufferCursorPosition + 0
	
	lda JsonParser.getByte + 2
	sta debugBufferCursorPosition + 1
	
	pla
rts


.endweak
