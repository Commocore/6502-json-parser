﻿
; @access public
; @return A - zero flag status (use bne for true)
isJsonArrayCompleted
	jsr fastForwardSpaceCharacters
	jsr getByteProcedure
	cmp #RIGHT_SQUARE_BRACKET_CHAR
	bne ++
		lda currentNestingLevel
		beq +
			jsr walkForwardProcedure
+
		
		jmp exitFromJsonArrayOrObjectLoop
+

	lda #DO_NOT_EXIT_FROM_JSON_ARRAY_OR_OBJECT_LOOP
rts
