﻿
; @access public
; @needs advanceToMethodIterator.iteratorPointers to be set before
; @return void
expectJsonKey
	jsr getByteProcedure
	cmp #RIGHT_CURLY_BRACKET_CHAR
	bne +
		; End of the JSON object has been reached, so let's ignore all expected JSON keys until isJsonObjectCompleted will be called
		rts
+

	lda jsonKeyLoaded
	bne +
		; No JSON key have been put to the buffer yet, look for the first occurrence and put into the buffer
		; Then, iteration through all expected keys will happen before walking forward in the JSON data
		jsr lookForBufferKey
+

	jsr doesExpectedKeyMatch
	beq +
		; Both keys match, let's execute iterator method
		jsr advanceToMethodIterator
+
rts
