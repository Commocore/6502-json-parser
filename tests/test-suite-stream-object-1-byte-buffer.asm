﻿
	; Testing for parser that is allowed to fetch only a single byte from the hardware
	
	; Include c64unit
	.include "../vendor/c64unit/cross-assemblers/64tass/core6000.asm"
	
	; Init
	c64unit
	
	; Examine test cases
	examineTest testExceededKeyLength
	examineTest testKeyValuePair
	examineTest testArrayOfStrings
	examineTest testArrayOfStringsWithKey
	examineTest testArrayOfIntegers
	examineTest testArrayOfIntegersWithKey
	examineTest testArrayOfObjects
	examineTest testMultipleKeys
	examineTest testEmptyElements
	examineTest testEscapedQuotes
	examineTest testEscapedQuotesInAnyKey
	examineTest testEscapedQuotesInExpectedKey
	examineTest testAsciiToPetscii
	examineTest testAnyKeyObject
	examineTest testLongValues
	examineTest testResettingOutputLocation
	examineTest testJsonWithSpacesBetweenElements
	examineTest testSimilarKeys
	
	; If this point is reached, there were no assertion fails
	c64unitExit
	
	; Include domain logic, i.e. classes, methods and tables
	.include "../src/json-parser.asm"
	.include "data/data.asm"
	
	; Settings for stream parsing, with 1 byte long buffer
	.include "includes/set-1-byte-buffer-streamed-parser.asm"
	
	; Testsuite with all test cases
	.include "test-cases/test-exceeded-key-length.asm"
	.include "test-cases/test-key-value-pair.asm"
	.include "test-cases/test-array-of-strings.asm"
	.include "test-cases/test-array-of-strings-with-key.asm"
	.include "test-cases/test-array-of-integers.asm"
	.include "test-cases/test-array-of-integers-with-key.asm"
	.include "test-cases/test-array-of-objects.asm"
	.include "test-cases/test-multiple-keys.asm"
	.include "test-cases/test-empty-elements.asm"
	.include "test-cases/test-escaped-quotes.asm"
	.include "test-cases/test-escaped-quotes-in-any-key.asm"
	.include "test-cases/test-escaped-quotes-in-expected-key.asm"
	.include "test-cases/test-ascii-to-petscii.asm"
	.include "test-cases/test-any-key-object.asm"
	.include "test-cases/test-long-values.asm"
	.include "test-cases/test-resetting-output-location.asm"
	.include "test-cases/test-json-with-spaces-between-elements.asm"
	.include "test-cases/test-similar-keys.asm"
