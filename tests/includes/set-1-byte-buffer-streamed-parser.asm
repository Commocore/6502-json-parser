﻿
; Variables
streamSource = $03 ; word
bufferSource = $05 ; word

; Constants
BUFFER_SIZE = 1 ; how many bytes to keep in buffer
STATIC_PARSING = FALSE


; Load current byte to accumulator
; Note: If Y register is used in this method, you have to save it, as JSON Parser uses it as well
; 
; @override
; @return A
JsonParser.getByte
	lda buffer
rts


; Simulate reading the next byte from stream
; 
; @override
; @return void
JsonParser.walkForward
	; Simulate requesting the next byte from stream
	jsr fillBufferFromStream

	; Simulate reading from the real hardware by moving pointers of the stream for further iterations
	lda streamSource + 0
	clc
	adc #<BUFFER_SIZE
	sta streamSource + 0
	lda streamSource + 1
	adc #>BUFFER_SIZE
	sta streamSource + 1
rts


; Simulate reading from stream
; 
; @access public
; @return void
fillBufferFromStream
	tya
	pha ; preserve Y
		
	ldy #0
	lda (streamSource),y
	sta buffer
	
	; Used only to assert in tests the last read byte from the stream
	lda streamSource + 0
	sta lastReadStreamByte + 0
	lda streamSource + 1
	sta lastReadStreamByte + 1
	
	pla ; restore Y
	tay
rts


; Handler function to imitate the stream
; 
; @access private
; @param A - stream source lo-byte
; @param X - stream source hi-byte
; @return void
setStream
	; Set pointers to JSON object from data.asm file
	sta streamSource + 0
	stx streamSource + 1
	
	lda #<buffer
	sta bufferSource + 0
	lda #>buffer
	sta bufferSource + 1
rts


lastReadStreamByte
	.word 0
	

buffer
	.byte 0
