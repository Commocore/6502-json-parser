﻿
; Variables
bufferPosition = $02 ; byte
streamSource = $03 ; word
bufferSource = $05 ; word

; Constants
BUFFER_SIZE = 10 ; how many bytes to keep in buffer
STATIC_PARSING = FALSE


; Load current byte to accumulator
; Note: If Y register is used in this method, you have to save it, as JSON Parser uses it as well
; 
; @override
; @return A
JsonParser.getByte
	ldx bufferPosition
	lda buffer,x
rts


; Simulate reading the next 10 bytes from stream to a custom buffer if needed
; and walking forward through that buffer
; 
; @override
; @return void
JsonParser.walkForward
	; Check if any bytes has been loaded
	lda JsonParser.byteLoaded
	beq fetchNext10Bytes
	
	inc bufferPosition
	lda bufferPosition
	cmp #BUFFER_SIZE
	beq fetchNext10Bytes
rts


; @access private
; @return void
fetchNext10Bytes
	lda #0
	sta bufferPosition

	; Simulate requesting the next #BUFFER_SIZE byte(s) from stream
	jsr fillBufferFromStream
	
	; Simulate reading from the real hardware by moving pointers of the stream for further iterations
	lda streamSource + 0
	clc
	adc #<BUFFER_SIZE
	sta streamSource + 0
	lda streamSource + 1
	adc #>BUFFER_SIZE
	sta streamSource + 1
rts


; Simulate reading from stream
; 
; @access public
; @return void
fillBufferFromStream
	tya
	pha ; preserve Y
		
	ldy #0
-
	lda (streamSource),y
	sta (bufferSource),y
	iny
	cpy #BUFFER_SIZE
	bne -
	
	; Used only to assert in tests the last read byte from the stream
	tya
	clc
	adc streamSource + 0
	sta lastReadStreamByte + 0
	lda streamSource + 1
	adc #0
	sta lastReadStreamByte + 1
	
	pla ; restore Y
	tay
rts


; Handler function to imitate the stream
; 
; @access private
; @param A - stream source lo-byte
; @param X - stream source hi-byte
; @return void
setStream
	; Set pointers to JSON object from data.asm file
	sta streamSource + 0
	stx streamSource + 1
	
	lda #<buffer
	sta bufferSource + 0
	lda #>buffer
	sta bufferSource + 1
rts


lastReadStreamByte
	.word 0
	
	
buffer
	.fill BUFFER_SIZE, 0
