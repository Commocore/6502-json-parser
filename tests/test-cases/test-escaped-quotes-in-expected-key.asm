﻿
; @access public
; @return void
testEscapedQuotesInExpectedKey .block
	.if STATIC_PARSING
		setJsonObject jsonObjectEscapedQuotesInKey ; set JSON object from data.asm file
	.else
		lda #<jsonObjectEscapedQuotesInKey
		ldx #>jsonObjectEscapedQuotesInKey
		jsr setStream
		setStreamedJsonObject
	.fi
	
-
	expectJsonKey '"light" blue', coloursIterator ; this one contains quote characters in key
	expectJsonKey "purple", coloursIterator
	expectJsonKey "cyan", coloursIterator
	isJsonObjectCompleted
	bne -

	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "esc quotes in exp key: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "esc quotes in exp key: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 12, "esc quotes in exp key: l. blue string"
	assertMemoryEqual purpleInteger, colourValuesTable + 1 * 0, 1, "esc quotes in exp key: purple int"
	assertMemoryEqual cyanInteger, colourValuesTable + 1 * 1, 1, "esc quotes in exp key: cyan int"
	assertMemoryEqual lightBlueInteger, colourValuesTable + 1 * 2, 2, "esc quotes in exp key: light blue int"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectEscapedQuotesInKeyEof - 1, JsonParser.getByte + 1, "esc quotes in exp key: buffer overflow" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectEscapedQuotesInKeyEof - 1, lastReadStreamByte, "esc quotes in exp key: buffer overflow" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectEscapedQuotesInKeyEof - jsonObjectEscapedQuotesInKey != 41, "Length of jsonObjectEscapedQuotesInKey JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectEscapedQuotesInKey + 10 * 5, lastReadStreamByte, "esc quotes in exp key: buffer overflow" ; the exact bytes read must match!
			; Data is 41 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 50 bytes 
		.fi
	.fi
rts


; @access private
; @return void
coloursIterator
	storeJsonKey coloursTableLocation, 16
	storeJsonValue colourValuesTableLocation, 1
rts
	

coloursTable
	.fill 16 * 3, 0
	
	
colourValuesTable
	.fill 1 * 3, 0
	
	
coloursTableLocation
	.word coloursTable
	
	
colourValuesTableLocation
	.word colourValuesTable
	

.enc "ascii"
.cdef 32, 126, 32

	
purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text '"light" blue'
	
	
purpleInteger
	.text '5'

	
cyanInteger
	.text '4'
	

lightBlueInteger
	.text '14'
	
.enc "none"
	
.bend
