﻿
; @access public
; @return void
testArrayOfIntegersWithKey .block
	.if STATIC_PARSING
		setJsonObject jsonObjectArrayOfIntegersWithKey ; set JSON object from data.asm file
	.else
		lda #<jsonObjectArrayOfIntegersWithKey
		ldx #>jsonObjectArrayOfIntegersWithKey
		jsr setStream
		setStreamedJsonObject
	.fi
	setJsonOutputMemoryLocation numbersTableLocation, numbersTable
	
	expectJsonKey "numbers", numbersIterator
	
	assertMemoryEqual firstNumberString, numbersTable + 8 * 0, 5, "test array of integers with key: #1"
	assertMemoryEqual secondNumberString, numbersTable + 8 * 1, 2, "test array of integers with key: #2"
	assertMemoryEqual thirdNumberString, numbersTable + 8 * 2, 4, "test array of integers with key: #3"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectArrayOfIntegersWithKeyEof - 1, JsonParser.getByte + 1, "array of ints w/ key: buffer overflow" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectArrayOfIntegersWithKeyEof - 1, lastReadStreamByte, "array of ints w/ key: buffer overflow" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectArrayOfIntegersWithKeyEof - jsonObjectArrayOfIntegersWithKey != 27, "Length of jsonObjectArrayOfIntegersWithKey JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectArrayOfIntegersWithKey + 10 * 3, lastReadStreamByte, "array of ints w/ key: buffer overflow" ; the exact bytes read must match!
			; Data is 27 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 30 bytes 
		.fi
	.fi
rts


; @access private
; @return void
numbersIterator
-
	storeJsonValue numbersTableLocation, 8
	isJsonArrayCompleted
	bne -
rts


numbersTable
	.fill 8 * 3, 0
	
	
numbersTableLocation
	.word 0
	

.enc "ascii"
.cdef 32, 126, 32

firstNumberString
	.text "16.05"

	
secondNumberString
	.text "64"

	
thirdNumberString
	.text "1024"
	

.enc "none"
	
.bend
