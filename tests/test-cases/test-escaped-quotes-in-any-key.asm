﻿
; @access public
; @return void
testEscapedQuotesInAnyKey .block
	.if STATIC_PARSING
		setJsonObject jsonObjectEscapedQuotesInKey ; set JSON object from data.asm file
	.else
		lda #<jsonObjectEscapedQuotesInKey
		ldx #>jsonObjectEscapedQuotesInKey
		jsr setStream
		setStreamedJsonObject
	.fi

-
	expectAnyJsonKey coloursIterator
	isJsonObjectCompleted
	bne -

	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "test esc quotes in key: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "test esc quotes in key: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 12, "test esc quotes in key: l. blue string"
	
	assertMemoryEqual purpleInteger, colourValuesTable + 2 * 0, 1, "test esc quotes in key: purple int"
	assertMemoryEqual cyanInteger, colourValuesTable + 2 * 1, 1, "test esc quotes in key: cyan int"
	assertMemoryEqual lightBlueInteger, colourValuesTable + 2 * 2, 2, "test esc quotes in key: light blue int"
	
	assertEqual #6, keyLengthTable + 0, "test esc quotes in key: purple length"
	assertEqual #4, keyLengthTable + 1, "test esc quotes in key: cyan length"
	assertEqual #12, keyLengthTable + 2, "test esc quotes in key: l. blue length"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectEscapedQuotesInKeyEof - 1, JsonParser.getByte + 1, "test esc quotes in key: buffer overflow" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectEscapedQuotesInKeyEof - 1, lastReadStreamByte, "test esc quotes in key: buffer overflow" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectEscapedQuotesInKeyEof - jsonObjectEscapedQuotesInKey != 41, "Length of jsonObjectEscapedQuotesInKey JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectEscapedQuotesInKey + 10 * 5, lastReadStreamByte, "test esc quotes in key: buffer overflow" ; the exact bytes read must match!
			; Data is 41 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 50 bytes 
		.fi
	.fi
rts


; @access private
; @return void
coloursIterator
	storeJsonKey coloursTableLocation, 16
	storeJsonKeyLength keyLengthTableLocation, 1
	storeJsonValue colourValuesTableLocation, 2
rts
	

coloursTable
	.fill 16 * 3, 0
	
	
colourValuesTable
	.fill 2 * 3, 0
	
	
keyLengthTable
	.fill 1 * 3, 0
	
	
coloursTableLocation
	.word coloursTable
	
	
colourValuesTableLocation
	.word colourValuesTable
	

keyLengthTableLocation
	.word keyLengthTable
	
	
.enc "ascii"
.cdef 32, 126, 32

	
purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text '"light" blue'
	
	
purpleInteger
	.text '5'

	
cyanInteger
	.text '4'
	

lightBlueInteger
	.text '14'
	
.enc "none"
	
.bend
