﻿
; @access public
; @return void
testAsciiToPetscii .block
	.if STATIC_PARSING
		setJsonObject jsonObjectAsciiToPetscii ; set JSON object from data.asm file
	.else
		lda #<jsonObjectAsciiToPetscii
		ldx #>jsonObjectAsciiToPetscii
		jsr setStream
		setStreamedJsonObject
	.fi
	setPetsciiMode
	
-
	expectJsonKey "alphabet_lowercase", alphabetLowercaseIterator
	expectJsonKey "alphabet_uppercase", alphabetUppercaseIterator
	expectJsonKey "integers", integersIterator
	expectJsonKey "special_characters", specialCharactersIterator
	isJsonObjectCompleted
	beq +
		jmp -
+

	assertMemoryEqual alphabetLowercaseString, alphabetLowercaseTable, size(alphabetLowercaseString), "test ascii - petscii: alphabet lowercase"
	assertMemoryEqual alphabetUppercaseString, alphabetUppercaseTable , size(alphabetUppercaseString), "test ascii - petscii: alphabet uppercase"
	assertMemoryEqual integersString, integersTable, size(integersString), "test ascii - petscii: integers"
	assertMemoryEqual specialCharactersString, specialCharactersTable, specialCharactersStringEnd - specialCharactersString, "test ascii - petscii: special characters"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectAsciiToPetsciiEof - 1, JsonParser.getByte + 1, "test ascii petscii: buffer overflow" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectAsciiToPetsciiEof - 1, lastReadStreamByte, "test ascii petscii: buffer overflow" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectAsciiToPetsciiEof - jsonObjectAsciiToPetscii != 177, "Length of jsonObjectAsciiToPetscii JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectAsciiToPetscii + 10 * 18, lastReadStreamByte, "test ascii petscii: buffer overflow" ; the exact bytes read must match!
			; Data is 177 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 180 bytes 
		.fi
	.fi
rts


; @access private
; @return void
alphabetLowercaseIterator
	storeJsonValue alphabetLowercaseTableLocation
rts


; @access private
; @return void
alphabetUppercaseIterator
	storeJsonValue alphabetUppercaseTableLocation
rts


; @access private
; @return void
integersIterator
	storeJsonValue integersTableLocation
rts


; @access private
; @return void
specialCharactersIterator
	storeJsonValue specialCharactersTableLocation
rts


alphabetLowercaseTable
	.fill 40, 0
	
	
alphabetUppercaseTable
	.fill 40, 0


integersTable
	.fill 40, 0


specialCharactersTable
	.fill 40, 0


alphabetLowercaseTableLocation
	.word alphabetLowercaseTable
	
	
alphabetUppercaseTableLocation
	.word alphabetUppercaseTable
	
	
integersTableLocation
	.word integersTable
	
	
specialCharactersTableLocation
	.word specialCharactersTable
	

.enc "screen"

alphabetLowercaseString .text "abcdefghijklmnopqrstuvwxyz"
	
	
alphabetUppercaseString .text "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


integersString .text "0123456789"


specialCharactersString
	.text '!@"#$%'
	.byte 30 ; up-arrow character
	.text '&*()[]<>/?'
	.byte 100 ; underscore character
	.text '=+-:;,.'
	.byte 39 ; apostrophe character

specialCharactersStringEnd

.enc "none"

	
.bend
