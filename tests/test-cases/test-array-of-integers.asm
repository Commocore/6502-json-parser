﻿
; @access public
; @return void
testArrayOfIntegers .block
	.if STATIC_PARSING
		setJsonObject jsonObjectArrayOfIntegers ; set JSON object from data.asm file
	.else
		lda #<jsonObjectArrayOfIntegers
		ldx #>jsonObjectArrayOfIntegers
		jsr setStream
		setStreamedJsonObject
	.fi
	setJsonOutputMemoryLocation numbersTableLocation, numbersTable
	setJsonOutputMemoryLocation lengthTableLocation, lengthTable
	
	jsr numbersIterator
	
	assertMemoryEqual firstNumberString, numbersTable + 8 * 0, 5, "test array of integers: #1"
	assertMemoryEqual secondNumberString, numbersTable + 8 * 1, 2, "test array of integers: #2"
	assertMemoryEqual thirdNumberString, numbersTable + 8 * 2, 4, "test array of integers: #3"
	assertWordEqual 5, lengthTable + 2 * 0, "test array of integers length: #1"
	assertWordEqual 2, lengthTable + 2 * 1, "test array of integers length: #2"
	assertWordEqual 4, lengthTable + 2 * 2, "test array of integers length: #3"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectArrayOfIntegersEof - 1, JsonParser.getByte + 1, "test array of integers: buffer overflow" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectArrayOfIntegersEof - 1, lastReadStreamByte, "test array of integers: buffer overflow" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectArrayOfIntegersEof - jsonObjectArrayOfIntegers != 15, "Length of jsonObjectArrayOfIntegers JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectArrayOfIntegers + 10 * 2, lastReadStreamByte, "test array of integers: buffer overflow" ; the exact bytes read must match!
			; Data is 15 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 20 bytes 
		.fi
	.fi
rts


; @access private
; @return void
numbersIterator
-
	storeJsonValue numbersTableLocation, 8
	storeJsonValueLength lengthTableLocation, 2
	isJsonArrayCompleted
	bne -
rts


numbersTable
	.fill 8 * 3, 0
	
	
lengthTable
	.fill 2 * 3, 0
	
	
numbersTableLocation
	.word 0
	
	
lengthTableLocation
	.word 0
	

.enc "ascii"
.cdef 32, 126, 32

firstNumberString
	.text "16.05"

	
secondNumberString
	.text "64"

	
thirdNumberString
	.text "1024"
	

.enc "none"
	
.bend
