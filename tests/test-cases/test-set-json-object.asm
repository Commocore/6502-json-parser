﻿
; For static JSON parsing, position of JSON object is one less as walkForward will walk to the first byte on start
; 
; @access public
; @return void
testSetJsonObject
	.cerror !STATIC_PARSING, "testSetJsonObject makes sense only for static testing"
	setJsonObject jsonObjectKeyValuePair ; set JSON object from data.asm file
	
	assertWordEqual jsonObjectKeyValuePair - 1, JsonParser.jsonObjectMemoryLocation, "test set json object"
	assertWordEqual jsonObjectKeyValuePair - 1, JsonParser.getByte + 1, "test set json object: starting byte" ; the exact bytes read must match!
rts
