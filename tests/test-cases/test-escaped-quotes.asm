﻿
; @access public
; @return void
testEscapedQuotes .block
	.if STATIC_PARSING
		setJsonObject jsonObjectEscapedQuotes ; set JSON object from data.asm file
	.else
		lda #<jsonObjectEscapedQuotes
		ldx #>jsonObjectEscapedQuotes
		jsr setStream
		setStreamedJsonObject
	.fi
	
-
	expectJsonKey "colours", coloursIterator
	expectJsonKey "i_just_wanted_to_say", sayIterator
	
	isJsonObjectCompleted
	bne -

	assertMemoryEqual saidString, saidTable, 28, "test escaped quotes: said string"
	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "test escaped quotes: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "test escaped quotes: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 12, "test escaped quotes: light blue string"
	assertWordEqual 28, lengthTable + 2 * 0, "test escaped quotes: said length"
	assertWordEqual 6, lengthTable + 2 * 1, "test escaped quotes: purple length"
	assertWordEqual 4, lengthTable + 2 * 2, "test escaped quotes: cyan length"
	assertWordEqual 12, lengthTable + 2 * 3, "test escaped quotes: light blue length"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectEscapedQuotesEof - 1, JsonParser.getByte + 1, "test escaped quotes: buffer overflow" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectEscapedQuotesEof - 1, lastReadStreamByte, "test escaped quotes: buffer overflow" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectEscapedQuotesEof - jsonObjectEscapedQuotes != 102, "Length of jsonObjectEscapedQuotes JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectEscapedQuotes + 10 * 11, lastReadStreamByte, "test escaped quotes: buffer overflow" ; the exact bytes read must match!
			; Data is 102 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 110 bytes 
		.fi
	.fi
rts


; @access private
; @return void
sayIterator
	storeJsonValue saidTableLocation
	storeJsonValueLength lengthTableLocation, 2
rts


; @access private
; @return void
coloursIterator
-
	storeJsonValue coloursTableLocation, 16
	storeJsonValueLength lengthTableLocation, 2
	isJsonArrayCompleted
	beq +
		jmp -
+
rts


saidTable
	.fill 32, 0
	

coloursTable
	.fill 16 * 3, 0
	
	
lengthTable
	.fill 2 * 4, 0
	
	
saidTableLocation
	.word saidTable
	
	
coloursTableLocation
	.word coloursTable
	
	
lengthTableLocation
	.word lengthTable
	
	
.enc "ascii"
.cdef 32, 126, 32

saidString
	.text 'someone put "me" in quotes?!'
	

purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text '"light" blue'
	

.enc "none"
	
.bend
