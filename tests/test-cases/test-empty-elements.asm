﻿
; This is a special example to demonstrate the importance of calling nested iterators properly.
; Even if we're not going to do anything with date object in this example, if there is a call to dateIterator with this method:
; 
; expectJsonKey "date", dateIterator
; 
; then JSON Parser will enter this element.
; As parsing will begin in this nested element, data needs to be processed with expectJsonValue, or other function.
; In other words, if you're not going to process data of this element, just don't enter it...
; Otherwise parsing will produce unexpected side-effects. Uncomment expectJsonKey function in testExpectedElementExample to find it out.
;
; This test also tests empty objects, empty arrays and empty strings 
; 
; @access public
; @return void
testEmptyElements .block
	.if STATIC_PARSING
		setJsonObject jsonObjectExpectedElementExample ; set JSON object from data.asm file
	.else
		lda #<jsonObjectExpectedElementExample
		ldx #>jsonObjectExpectedElementExample
		jsr setStream
		setStreamedJsonObject
	.fi
	
-
	expectJsonKey	"colours", coloursIterator
	;expectJsonKey "date", dateIterator ; intentionally commented out, please uncomment to see unexpected behaviour
	expectJsonKey "empty_array", emptyArrayIterator
	expectJsonKey "empty_object", emptyObjectIterator
	expectJsonKey "empty_string", emptyStringIterator
	
	isJsonObjectCompleted
	beq +
		jmp -
+

	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "test empty elements: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "test empty elements: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 10, "test empty elements: light blue string"
	assertEqual #0, emptyArrayTable, "test empty elements: empty array"
	assertEqual #0, emptyObjectTable, "test empty elements: empty object"
	assertEqual #0, emptyStringTable, "test empty elements: empty string"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectExpectedElementExampleEof - 1, JsonParser.getByte + 1, "test empty elements: buffer overflow" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectExpectedElementExampleEof - 1, lastReadStreamByte, "test empty elements: buffer overflow" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectExpectedElementExampleEof - jsonObjectExpectedElementExample != 115, "Length of jsonObjectExpectedElementExample JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectExpectedElementExample + 10 * 12, lastReadStreamByte, "test empty elements: buffer overflow" ; the exact bytes read must match!
			; Data is 115 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 120 bytes 
		.fi
	.fi
rts


; @access private
; @return void
dateIterator
	; Even if we're not going to do anything here with the value, dateIterator method has been already called,
	; so parser is nested into this element and you have to store values available inside using storeJsonValue, or iterate further
rts


; @access private
; @return void
emptyArrayIterator
	storeJsonValue emptyArrayLocation
rts


; @access private
; @return void
emptyObjectIterator
	storeJsonValue emptyObjectLocation
rts


; @access private
; @return void
emptyStringIterator
	storeJsonValue emptyStringLocation
rts


; @access private
; @return void
coloursIterator
-
	storeJsonValue coloursTableLocation, 16
	isJsonArrayCompleted
	bne -
rts


coloursTable
	.fill 16 * 3, 0
	

emptyArrayTable
	.byte 0
	
	
emptyObjectTable
	.byte 0
	

emptyStringTable
	.byte 0
	
	
coloursTableLocation
	.word coloursTable
	
	
emptyArrayLocation
	.word emptyArrayTable
	
	
emptyObjectLocation
	.word emptyObjectTable
	
	
emptyStringLocation
	.word emptyStringTable
	

.enc "ascii"
.cdef 32, 126, 32

purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text "light blue"
	

.enc "none"
	
.bend
