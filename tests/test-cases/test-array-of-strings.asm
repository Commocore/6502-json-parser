﻿
; @access public
; @return void
testArrayOfStrings .block
	.if STATIC_PARSING
		setJsonObject jsonObjectArrayOfStrings ; set JSON object from data.asm file
	.else
		lda #<jsonObjectArrayOfStrings
		ldx #>jsonObjectArrayOfStrings
		jsr setStream
		setStreamedJsonObject
	.fi
	setJsonOutputMemoryLocation coloursTableLocation, coloursTable
	setJsonOutputMemoryLocation lengthTableLocation, lengthTable
	
	jsr coloursIterator

	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "test array of strings: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "test array of strings: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 10, "test array of strings: light blue string"
	assertWordEqual 6, lengthTable + 2 * 0, "test array of strings: purple length"
	assertWordEqual 4, lengthTable + 2 * 1, "test array of strings: cyan length"
	assertWordEqual 10, lengthTable + 2 * 2, "test array of strings: light blue length"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectArrayOfStringsEof - 1, JsonParser.getByte + 1, "test array of strings: buffer overflow" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectArrayOfStringsEof - 1, lastReadStreamByte, "test array of strings: buffer overflow" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectArrayOfStringsEof - jsonObjectArrayOfStrings != 30, "Length of jsonObjectArrayOfStrings JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectArrayOfStrings + 10 * 3, lastReadStreamByte, "test array of strings: buffer overflow" ; the exact bytes read must match!
			; Data is 30 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 30 bytes 
		.fi
	.fi
rts


; @access private
; @return void
coloursIterator
-
	storeJsonValue coloursTableLocation, 16
	storeJsonValueLength lengthTableLocation, 2
	isJsonArrayCompleted
	bne -
rts


coloursTable
	.fill 16 * 3, 0
	
	
lengthTable
	.fill 2 * 3, 0
	

coloursTableLocation
	.word 0
	
	
lengthTableLocation
	.word 0
	

.enc "ascii"
.cdef 32, 126, 32

purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text "light blue"
	

.enc "none"
	
.bend
