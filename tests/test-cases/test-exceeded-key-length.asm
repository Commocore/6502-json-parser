﻿
; @access public
; @return void
testExceededKeyLength .block
	.if STATIC_PARSING
		setJsonObject jsonObjectExceedKeyLength ; set JSON object from data.asm file
	.else
		lda #<jsonObjectExceedKeyLength
		ldx #>jsonObjectExceedKeyLength
		jsr setStream
		setStreamedJsonObject
	.fi
	
	expectJsonKey "first_96_bytes_of_this_very_very_very_long_key_are_going_to_be_parsed_and_the_rest_will_be_just_", longKeyIterator
	
	assertMemoryEqual expectedLongKeyString, longKeyTable, 3, "test exceeded key: long key #1 string"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectExceedKeyLengthEof - 1, JsonParser.getByte + 1, "test exceeded key: buffer overflow #1" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectExceedKeyLengthEof - 1, lastReadStreamByte, "test exceeded key: buffer overflow #1" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectExceedKeyLengthEof - jsonObjectExceedKeyLength != 125, "Length of jsonObjectExceedKeyLength JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectExceedKeyLength + 10 * 13, lastReadStreamByte, "test exceeded key: buffer overflow #1" ; the exact bytes read must match!
			; Data is 125 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 130 bytes 
		.fi
	.fi
	
	; Clean table, and try again with longer key value. As you see, still only the first 96 bytes are going to be matched.
	; You can increase the value of JSON_KEY_MAX_LENGTH constant up to 255 characters
	ldy #14
-
	lda #0
	sta longKeyTable,y
	dey
	bpl -
	
	.if STATIC_PARSING
		setJsonObject jsonObjectExceedKeyLength ; set JSON object from data.asm file
	.else
		lda #<jsonObjectExceedKeyLength
		ldx #>jsonObjectExceedKeyLength
		jsr setStream
		setStreamedJsonObject
	.fi
	
	expectJsonKey "first_96_bytes_of_this_very_very_very_long_key_are_going_to_be_parsed_and_the_rest_will_be_just_ignored", longKeyIterator
	
	assertMemoryEqual expectedLongKeyString, longKeyTable, 15, "test exceeded key: long key #2 string"

	.if STATIC_PARSING
		assertWordEqual jsonObjectExceedKeyLengthEof - 1, JsonParser.getByte + 1, "test exceeded key: buffer overflow #2" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectExceedKeyLengthEof - 1, lastReadStreamByte, "test exceeded key: buffer overflow #2" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			assertWordEqual jsonObjectExceedKeyLength + 10 * 13, lastReadStreamByte, "test exceeded key: buffer overflow #2" ; the exact bytes read must match!
			; Data is 125 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 130 bytes 
		.fi
	.fi
rts


; @access private
; @return void
longKeyIterator
	storeJsonValue longKeyTableLocation
rts


longKeyTable
	.fill 15, 0
	
	
longKeyTableLocation
	.word longKeyTable
	

.enc "ascii"
.cdef 32, 126, 32

expectedLongKeyString
	.text "let me think..."


.enc "none"
	
.bend
