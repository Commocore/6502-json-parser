﻿
; @access public
; @return void
testSimilarKeys .block
	.if STATIC_PARSING
		setJsonObject jsonObjectSimilarKeys ; set JSON object from data.asm file
	.else
		lda #<jsonObjectSimilarKeys
		ldx #>jsonObjectSimilarKeys
		jsr setStream
		setStreamedJsonObject
	.fi

-
	expectJsonKey "numbers", numbersIterator
	expectJsonKey "number", numberIterator

	isJsonObjectCompleted
	bne -
	
	assertMemoryEqual expectedNumber, numberTable, 1, "test similar keys: number"
	assertMemoryEqual expectedNumbers, numbersTable, 1, "test similar keys: numbers"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectSimilarKeysEof - 1, JsonParser.getByte + 1, "test similar keys: buffer overflow" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectSimilarKeysEof - 1, lastReadStreamByte, "test similar keys: buffer overflow" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectSimilarKeysEof - jsonObjectSimilarKeys != 28, "Length of jsonObjectSimilarKeys JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectSimilarKeys + 10 * 3, lastReadStreamByte, "test similar keys: buffer overflow" ; the exact bytes read must match!
			; Data is 28 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 30 bytes 
		.fi
	.fi
rts


; @access private
; @return void
numberIterator
	storeJsonValue numberTableLocation
rts


; @access private
; @return void
numbersIterator
	storeJsonValue numbersTableLocation
rts


numberTable
	.fill 1, 0
	
	
numbersTable
	.fill 1, 0
	
	
numberTableLocation
	.word numberTable
	
	
numbersTableLocation
	.word numbersTable
	

.enc "ascii"
.cdef 32, 126, 32

expectedNumber
	.text "1"
	
	
expectedNumbers
	.text "6"
	

.enc "none"
	
.bend
