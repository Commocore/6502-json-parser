﻿
; @access public
; @return void
testKeyValuePair .block
	.if STATIC_PARSING
		setJsonObject jsonObjectKeyValuePair ; set JSON object from data.asm file
	.else
		lda #<jsonObjectKeyValuePair
		ldx #>jsonObjectKeyValuePair
		jsr setStream
		setStreamedJsonObject
	.fi
	
-
	expectJsonKey "some_non_existing_key", unusedKeyIterator
	expectJsonKey "date", dateIterator
	expectJsonKey "some_other_non_existing_key", unusedKeyIterator

	isJsonObjectCompleted
	bne -

	assertMemoryEqual dateString, dateTable, 10, "test key-value pair: date string"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectKeyValuePairEof - 1, JsonParser.getByte + 1, "test key-value pair: buffer overflow" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectKeyValuePairEof - 1, lastReadStreamByte, "test key-value pair: buffer overflow" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectKeyValuePairEof - jsonObjectKeyValuePair != 21, "Length of jsonObjectKeyValuePair JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectKeyValuePair + 10 * 3, lastReadStreamByte, "test key-value pair: buffer overflow" ; the exact bytes read must match!
			; Data is 21 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 30 bytes 
		.fi
	.fi
rts


; @access private
; @return void
dateIterator
	storeJsonValue dateTableLocation
rts


; @access private
; @return void
unusedKeyIterator
	; intentionally left blank
rts


dateTable
	.fill 10, 0
	
	
dateTableLocation
	.word dateTable
	

.enc "ascii"
.cdef 32, 126, 32

dateString
	.text "2017-08-13"
	

.enc "none"
	
.bend
