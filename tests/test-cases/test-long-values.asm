﻿
; @access public
; @return void
testLongValues .block
	.if STATIC_PARSING
		setJsonObject jsonObjectLongValues ; set JSON object from data.asm file
	.else
		lda #<jsonObjectLongValues
		ldx #>jsonObjectLongValues
		jsr setStream
		setStreamedJsonObject
	.fi
	
-
	expectJsonKey "colours", coloursIterator

	isJsonObjectCompleted
	bne -
	
	assertMemoryEqual coloursString, coloursTable, coloursStringEnd - coloursString, "test long values: colours string"
	assertWordEqual coloursStringEnd - coloursString, lengthTable, "test long values: length"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectLongValuesEof - 1, JsonParser.getByte + 1, "test long values: buffer overflow" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectLongValuesEof - 1, lastReadStreamByte, "test long values: buffer overflow" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectLongValuesEof - jsonObjectLongValues != 1374, "Length of jsonObjectLongValues JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectLongValues + 10 * 138, lastReadStreamByte, "test long values: buffer overflow" ; the exact bytes read must match!
			; Data is 1374 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 1380 bytes 
		.fi
	.fi
rts


; @access private
; @return void
coloursIterator
	storeJsonValue coloursTableLocation
	storeJsonValueLength lengthTableLocation
rts


coloursTable
	.fill coloursStringEnd - coloursString, 0
	
	
lengthTable
	.word 0
	
	
coloursTableLocation
	.word coloursTable
	
	
lengthTableLocation
	.word lengthTable

	
.enc "ascii"
.cdef 32, 126, 32


coloursString
	.for i=0, i<10, i=i+1
		.text 'black, white, red, cyan, purple, green, blue, yellow, '
		.text 'orange, brown, pink, dark gray, medium gray, light green, light blue, light gray, '
	.next
	
coloursStringEnd
	

.enc "none"
	
.bend
