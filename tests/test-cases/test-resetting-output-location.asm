﻿
; @access public
; @return void
testResettingOutputLocation .block
	.if STATIC_PARSING
		setJsonObject jsonObjectArrayOfStringsWithKey ; set JSON object from data.asm file
	.else
		lda #<jsonObjectArrayOfStringsWithKey
		ldx #>jsonObjectArrayOfStringsWithKey
		jsr setStream
		setStreamedJsonObject
	.fi
	setJsonOutputMemoryLocation coloursTableLocation, coloursTable
	
-
	expectJsonKey "colours", coloursIterator
	isJsonObjectCompleted
	bne -

	; Assert
	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "test array of strings w/ key: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "test array of strings w/ key: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 10, "test array of strings w/ key: l. blue string"

	.if STATIC_PARSING
		assertWordEqual jsonObjectArrayOfStringsWithKeyEof - 1, JsonParser.getByte + 1, "array of str w/ key: buffer overflow #1" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectArrayOfStringsWithKeyEof - 1, lastReadStreamByte, "array of str w/ key: buffer overflow #1" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectArrayOfStringsWithKeyEof - jsonObjectArrayOfStringsWithKey != 42, "Length of jsonObjectArrayOfStringsWithKey JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectArrayOfStringsWithKey + 10 * 5, lastReadStreamByte, "array of str w/ key: buffer overflow #1" ; the exact bytes read must match!
			; Data is 42 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 50 bytes 
		.fi
	.fi

	
	; Repeat again
	jsr clearOutputMemoryLocation
	.if STATIC_PARSING
		setJsonObject jsonObjectArrayOfStringsWithKey ; set JSON object from data.asm file
	.else
		lda #<jsonObjectArrayOfStringsWithKey
		ldx #>jsonObjectArrayOfStringsWithKey
		jsr setStream
		setStreamedJsonObject
	.fi
	setJsonOutputMemoryLocation coloursTableLocation, coloursTable
-
	expectJsonKey "colours", coloursIterator
	isJsonObjectCompleted
	bne -
	
	; Assert
	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "reset output location: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "reset output location: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 10, "reset output location: l. blue string"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectArrayOfStringsWithKeyEof - 1, JsonParser.getByte + 1, "array of str w/ key: buffer overflow #2" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectArrayOfStringsWithKeyEof - 1, lastReadStreamByte, "array of str w/ key: buffer overflow #2" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			assertWordEqual jsonObjectArrayOfStringsWithKey + 10 * 5, lastReadStreamByte, "array of str w/ key: buffer overflow #2" ; the exact bytes read must match!
			; Data is 42 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 50 bytes 
		.fi
	.fi
	
	; Repeat again
	jsr clearOutputMemoryLocation
	.if STATIC_PARSING
		setJsonObject jsonObjectArrayOfStringsWithKey ; set JSON object from data.asm file
	.else
		lda #<jsonObjectArrayOfStringsWithKey
		ldx #>jsonObjectArrayOfStringsWithKey
		jsr setStream
		setStreamedJsonObject
	.fi
	setJsonOutputMemoryLocation coloursTableLocation, coloursTable
-
	expectJsonKey "colours", coloursIterator
	isJsonObjectCompleted
	bne -
	
	; Assert
	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "test array of strings w/ key: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "test array of strings w/ key: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 10, "test array of strings w/ key: l. blue string"

	.if STATIC_PARSING
		assertWordEqual jsonObjectArrayOfStringsWithKeyEof - 1, JsonParser.getByte + 1, "array of str w/ key: buffer overflow #3" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectArrayOfStringsWithKeyEof - 1, lastReadStreamByte, "array of str w/ key: buffer overflow #3" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			assertWordEqual jsonObjectArrayOfStringsWithKey + 10 * 5, lastReadStreamByte, "array of str w/ key: buffer overflow #3" ; the exact bytes read must match!
			; Data is 42 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 50 bytes 
		.fi
	.fi
rts


; @access private
; @return void
coloursIterator
-
	storeJsonValue coloursTableLocation, 16
	isJsonArrayCompleted
	bne -
rts


; Internal method to clean output memory location for the next run
;
; @access private
; @return void
clearOutputMemoryLocation
	lda #<coloursTable
	sta clearPointer+1
	lda #>coloursTable
	sta clearPointer+2
	
	ldy #16 * 3 - 1
	lda #0
clearPointer
	sta $1234,y
	dey
	bpl clearPointer
rts


coloursTable
	.fill 16 * 3, 0
	
	
coloursTableLocation
	.word 0
	

.enc "ascii"
.cdef 32, 126, 32

purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text "light blue"
	

.enc "none"
	
.bend
