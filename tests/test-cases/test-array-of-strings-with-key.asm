﻿
; @access public
; @return void
testArrayOfStringsWithKey .block
	.if STATIC_PARSING
		setJsonObject jsonObjectArrayOfStringsWithKey ; set JSON object from data.asm file
	.else
		lda #<jsonObjectArrayOfStringsWithKey
		ldx #>jsonObjectArrayOfStringsWithKey
		jsr setStream
		setStreamedJsonObject
	.fi
	setJsonOutputMemoryLocation coloursTableLocation, coloursTable

-
	expectJsonKey "some_non_existing_key", unusedKeyIterator
	expectJsonKey "colours", coloursIterator
	expectJsonKey "some_other_non_existing_key", unusedKeyIterator

	isJsonObjectCompleted
	bne -

	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "array of strings w/ key: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "array of strings w/ key: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 10, "array of strings w/ key: l. blue string"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectArrayOfStringsWithKeyEof - 1, JsonParser.getByte + 1, "test array of str w/key: buffer overflow" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectArrayOfStringsWithKeyEof - 1, lastReadStreamByte, "test array of str w/key: buffer overflow" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectArrayOfStringsWithKeyEof - jsonObjectArrayOfStringsWithKey != 42, "Length of jsonObjectArrayOfStringsWithKey JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectArrayOfStringsWithKey + 10 * 5, lastReadStreamByte, "test array of str w/key: buffer overflow" ; the exact bytes read must match!
			; Data is 42 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 50 bytes 
		.fi
	.fi
rts


; @access private
; @return void
coloursIterator	
-
	storeJsonValue coloursTableLocation, 16
	isJsonArrayCompleted
	bne -
rts


; @access private
; @return void
unusedKeyIterator
	; intentionally left blank
rts


coloursTable
	.fill 16 * 3, 0
	
	
coloursTableLocation
	.word 0
	

.enc "ascii"
.cdef 32, 126, 32

purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text "light blue"
	

.enc "none"
	
.bend
