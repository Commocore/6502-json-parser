﻿
; @access public
; @return void
testJsonWithSpacesBetweenElements .block
	.if STATIC_PARSING
		setJsonObject jsonObjectMultipleKeysWithSpacesBetweenElements ; set JSON object from data.asm file
	.else
		lda #<jsonObjectMultipleKeysWithSpacesBetweenElements
		ldx #>jsonObjectMultipleKeysWithSpacesBetweenElements
		jsr setStream
		setStreamedJsonObject
	.fi
	
-
	expectJsonKey "date", dateIterator
	expectJsonKey "shapes", shapesIterator
	expectJsonKey "colours", coloursIterator
	expectJsonKey "position", positionIterator
	expectJsonKey "numbers", numbersIterator
	isJsonObjectCompleted
	beq +
		jmp -
+

	assertMemoryEqual dateString, dateTable, 10, "test with spaces: date string"
	assertMemoryEqual cubeString, shapesNameTable + 16 * 0, 4, "test with spaces: cube string"
	assertMemoryEqual sphereString, shapesNameTable + 16 * 1, 6, "test with spaces: sphere string"
	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "test with spaces: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "test with spaces: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 10, "test with spaces: light blue string"
	assertMemoryEqual positionXString, positionXTable, 2, "test with spaces: position x string"
	assertMemoryEqual positionYString, positionYTable, 2, "test with spaces: position y string"
	assertMemoryEqual positionZString, positionZTable, 4, "test with spaces: position z string"
	assertMemoryEqual firstNumberString, numbersTable + 8 * 0, 5, "test with spaces: 1st number"
	assertMemoryEqual secondNumberString, numbersTable + 8 * 1, 2, "test with spaces: 2nd number"
	assertMemoryEqual thirdNumberString, numbersTable + 8 * 2, 4, "test with spaces: 3rd number"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectMultipleKeysWithSpacesBetweenElementsEof - 1, JsonParser.getByte + 1, "test with spaces: buffer overflow" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectMultipleKeysWithSpacesBetweenElementsEof - 1, lastReadStreamByte, "test with spaces: buffer overflow" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectMultipleKeysWithSpacesBetweenElementsEof - jsonObjectMultipleKeysWithSpacesBetweenElements != 291, "Length of jsonObjectMultipleKeysWithSpacesBetweenElements JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectMultipleKeysWithSpacesBetweenElements + 10 * 30, lastReadStreamByte, "test with spaces: buffer overflow" ; the exact bytes read must match!
			; Data is 291 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 300 bytes 
		.fi
	.fi
rts


; @access private
; @return void
dateIterator
	storeJsonValue dateTableLocation
rts


; @access private
; @return void
coloursIterator
-
	storeJsonValue coloursTableLocation, 16
	isJsonArrayCompleted
	bne -
rts


; @access private
; @return void
shapesIterator
-
	expectJsonKey "name", setSingleShapeName
	expectJsonKey "height", setSingleShapeHeight
	isJsonObjectCompleted
	bne -
	
	isJsonArrayCompleted
	bne -
rts


; @access private
; @return void
positionIterator
-
	expectJsonKey "x", setX
	expectJsonKey "z", setZ
	expectJsonKey "y", setY
	isJsonObjectCompleted
	bne -
rts


; @access private
; @return void
numbersIterator
-
	storeJsonValue numbersTableLocation, 8
	isJsonArrayCompleted
	bne -
rts


; @access private
; @return void
setSingleShapeName
	storeJsonValue shapesNameTableLocation, 16
rts


; @access private
; @return void
setSingleShapeWidth
	storeJsonValue shapesWidthTableLocation, 16
rts


; @access private
; @return void
setSingleShapeHeight
	storeJsonValue shapesHeightTableLocation, 16
rts


; @access private
; @return void
setX
	storeJsonValue positionXTableLocation
rts


; @access private
; @return void
setY
	storeJsonValue positionYTableLocation
rts


; @access private
; @return void
setZ
	storeJsonValue positionZTableLocation
rts


dateTable
	.fill 10, 0


coloursTable
	.fill 16 * 3, 0
	

shapesNameTable
	.fill 16 * 5, 0
	
	
shapesWidthTable
	.fill 16 * 5, 0
	
	
shapesHeightTable
	.fill 16 * 5, 0

	
positionXTable
	.fill 8, 0

	
positionYTable
	.fill 8, 0

	
positionZTable
	.fill 8, 0
	
	
numbersTable
	.fill 8 * 3, 0


dateTableLocation
	.word dateTable

	
coloursTableLocation
	.word coloursTable
	
	
shapesNameTableLocation
	.word shapesNameTable
	
	
shapesWidthTableLocation
	.word shapesWidthTable
	
	
shapesHeightTableLocation
	.word shapesHeightTable

	
positionXTableLocation
	.word positionXTable
	

positionYTableLocation
	.word positionYTable

	
positionZTableLocation
	.word positionZTable
	

numbersTableLocation
	.word numbersTable
	
	
.enc "ascii"
.cdef 32, 126, 32

dateString
	.text "2017-08-13"
	
	
cubeString
	.text "cube"


sphereString
	.text "sphere"


purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text "light blue"
	
	
positionXString
	.text "16"
	
	
positionYString
	.text "25"
	
	
positionZString
	.text "1000"
	

firstNumberString
	.text "16.05"

	
secondNumberString
	.text "64"

	
thirdNumberString
	.text "1029"
	
	
.enc "none"
	
.bend
