﻿
; @access public
; @return void
testAnyKeyObject .block
	.if STATIC_PARSING
		setJsonObject jsonObjectAnyKeyObject ; set JSON object from data.asm file
	.else
		lda #<jsonObjectAnyKeyObject
		ldx #>jsonObjectAnyKeyObject
		jsr setStream
		setStreamedJsonObject
	.fi
	setJsonOutputMemoryLocation namesTableLocation, scoreTable
	setJsonOutputMemoryLocation pointsTableLocation, scoreTable + 16
	setJsonOutputMemoryLocation keyLengthTableLocation, keyLengthTable

-
	expectAnyJsonKey scoreIterator
	isJsonObjectCompleted
	bne -

	assertMemoryEqual name1String, scoreTable + 32 * 0, 10, "test any key object: name #1"
	assertMemoryEqual name2String, scoreTable + 32 * 1, 13, "test any key object: name #2"
	assertMemoryEqual name3String, scoreTable + 32 * 2, 6, "test any key object: name #3"
	assertMemoryEqual score1String, scoreTable + 32 * 0 + 16, 5, "test any key object: score #1"
	assertMemoryEqual score2String, scoreTable + 32 * 1 + 16, 5, "test any key object: score #2"
	assertMemoryEqual score3String, scoreTable + 32 * 2 + 16, 5, "test any key object: score #3"
	assertEqual #10, keyLengthTable + 0, "test any key object: name #1 length"
	assertEqual #13, keyLengthTable + 1, "test any key object: name #2 length"
	assertEqual #6, keyLengthTable + 2, "test any key object: name #3 length"

	.if STATIC_PARSING
		assertWordEqual jsonObjectAnyKeyObjectEof - 1, JsonParser.getByte + 1, "test any key object: buffer overflow" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectAnyKeyObjectEof - 1, lastReadStreamByte, "test any key object: buffer overflow" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectAnyKeyObjectEof - jsonObjectAnyKeyObject != 57, "Length of jsonObjectAnyKeyObject JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectAnyKeyObject + 10 * 6, lastReadStreamByte, "test any key object: buffer overflow" ; the exact bytes read must match!
			; Data is 57 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 60 bytes 
		.fi
	.fi
rts


; @access private
; @return void
scoreIterator
	storeJsonKey namesTableLocation, 32
	storeJsonKeyLength keyLengthTableLocation, 1
	storeJsonValue pointsTableLocation, 32
rts


scoreTable
	.fill 32 * 3, 0
	

keyLengthTable
	.fill 1 * 3, 0
	
	
namesTableLocation
	.word 0
	
	
pointsTableLocation
	.word 0
	
	
keyLengthTableLocation
	.word 0
	

.enc "ascii"
.cdef 32, 126, 32

name1String
	.text "circus tom"

	
name2String
	.text "jet set willy"

	
name3String
	.text "slaine"
	

score1String
	.text "49500"
	

score2String
	.text "43750"
	

score3String
	.text "38550"
	
	
.enc "none"
	
.bend
