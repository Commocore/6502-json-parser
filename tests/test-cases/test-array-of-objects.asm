﻿
; @access public
; @return void
testArrayOfObjects .block
	.if STATIC_PARSING
		setJsonObject jsonObjectHiScores ; set JSON object from data.asm file
	.else
		lda #<jsonObjectHiScores
		ldx #>jsonObjectHiScores
		jsr setStream
		setStreamedJsonObject
	.fi

-
	expectJsonKey "player", playerIterator
	expectJsonKey "value", valueIterator
	isJsonObjectCompleted
	bne -
	
	isJsonArrayCompleted
	bne -

	assertMemoryEqual playerString1, playerTable, 7, "test array of objects: player1 string"
	assertMemoryEqual valueString1, valueTable, 5, "test array of objects: value1 string"
	assertMemoryEqual playerString2, playerTable + 10, 5, "test array of objects: player2 string"
	assertMemoryEqual valueString2, valueTable + 10, 5, "test array of objects: value2 string"
	assertWordEqual 7, playerNameLengthTable, "test array of objects: player1 length"
	assertWordEqual 5, playerNameLengthTable + 10, "test array of objects: player2 length"
	assertWordEqual 5, valueLengthTable, "test array of objects: value1 length"
	assertWordEqual 5, valueLengthTable + 10, "test array of objects: value2 length"
	
	.if STATIC_PARSING
		assertWordEqual jsonObjectHiScoresEof - 1, JsonParser.getByte + 1, "test array of objects: buffer overflow" ; the exact bytes read must match!
	.else
		.if BUFFER_SIZE = 1
			assertWordEqual jsonObjectHiScoresEof - 1, lastReadStreamByte, "test array of objects: buffer overflow" ; the exact bytes read must match!
		.elsif BUFFER_SIZE = 10
			.cerror jsonObjectHiScoresEof - jsonObjectHiScores != 133, "Length of jsonObjectHiScores JSON object doesn't match" ; pre-check as this will be important for a buffer assertion below
			assertWordEqual jsonObjectHiScores + 10 * 14, lastReadStreamByte, "test array of objects: buffer overflow" ; the exact bytes read must match!
			; Data is 133 bytes long, so as 10 bytes are read in advance into the buffer, the position of the streamSource should end up after 140 bytes 
		.fi
	.fi
rts


; @access private
; @return void
playerIterator
	storeJsonValue playerTableLocation, 10
	storeJsonValueLength playerNameLengthLocation, 10
rts


; @access private
; @return void
valueIterator
	storeJsonValue valueTableLocation, 10
	storeJsonValueLength valueLengthLocation, 10
rts


playerTable
	.fill 10 * 2, 0


valueTable
	.fill 10 * 2, 0


playerNameLengthTable
	.fill 10 * 2, 0
	
	
valueLengthTable
	.fill 10 * 2, 0


playerTableLocation
	.word playerTable

	
valueTableLocation
	.word valueTable
	
	
playerNameLengthLocation
	.word playerNameLengthTable
	
	
valueLengthLocation
	.word valueLengthTable
	
	
.enc "ascii"
.cdef 32, 126, 32

playerString1
	.text "gribbly"
	
	
playerString2
	.text "giana"
	
	
valueString1
	.text "65172"
	
	
valueString2
	.text "45387"

.enc "none"
	
.bend
