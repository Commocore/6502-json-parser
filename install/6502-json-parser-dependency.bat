@echo off
IF EXIST vendor\6502-json-parser (
    cd vendor\6502-json-parser
    git pull http://Commocore@bitbucket.org/Commocore/6502-json-parser.git master
    cd ..\..\
) ELSE (
    git clone --origin 6502-json-parser http://Commocore@bitbucket.org/Commocore/6502-json-parser.git vendor/6502-json-parser
)
@echo on
